.SUFFIXES: .c .obj .rc .res 

.all: ascii.exe

.rc.res:
    @echo " Compile::Resource Compiler "
    rc.exe -r %s %|dpfF.RES

.c.obj:
    @echo " Compile::C++ Compiler "
    icc.exe /DFULL /Tdp /O /Gm /C %s

ascii.exe: ascii.obj tool.obj ascii.res ascii.def
    @echo " Link::Linker "
    @echo " Bind::Resource Bind "
    icc.exe @<<
     /Feascii.exe 
     os2386.lib 
     cppom30.lib 
     ascii.def
     ascii.obj
     tool.obj
<<
    rc.exe ascii.res ascii.exe

ascii.res: ascii.rc ascii.ico ascii.h

ascii.obj: ascii.c ascii.h tool.h

tool.obj: tool.c ascii.h tool.h
